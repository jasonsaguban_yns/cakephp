<?php
class Post extends AppModel
{

    public $hasMany = array('Like','Comment','Repost');
    public $actsAs = array('CakeSoftDelete.SoftDeletable');
    public $validate = array(
        'post' => array(
            'rule' => 'notBlank'
        ),
        'user_id' => array(
            'rule' => 'numeric'
        ),
        'repost_id' => array(
            'rule' => 'numeric'
        )
    );

    public function isOwnedBy($post, $user)
    {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }
}
