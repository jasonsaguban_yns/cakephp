<?php

class Like extends AppModel
{
    public $validate = array(
        'post_id' => array(
            'rule' => 'numeric'
        ),
        'user_id' => array(
            'rule' => 'numeric'
        )
    );
}
