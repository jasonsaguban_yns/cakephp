<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{
    public $actsAs = array('CakeSoftDelete.SoftDeletable');

    public $validate = array(
        'username' => array(
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Letters and numbers only.',
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 15),
                'message' => 'Username must be from 5 to 15 characters only.',
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Enter a username.'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Username is already taken.'
            )
        ),
        'password' => array(
            'lenght' => array(
                'rule' => array('minLength', '8'),
                'message' => 'Minimum 8 characters long.',
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Enter a password.'
            )
        ),
        'first_name' => array(
            'lenght' => array(
                'rule' => '/^[a-zA-Z ]{0,}$/i',
                'message' => 'First name must be letters only'
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Enter your first name.'
            )
        ),
        'last_name' => array(
            'lenght' => array(
                'rule' => '/^[a-zA-Z ]{0,}$/i',
                'message' => 'Last name must be letters only'
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Enter your last name.'
            )
        ),
        'email' => array(
            'validEmail' => array(
                "rule" => 'email',
                'message' => 'Input a valid email'
            ),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'Enter your valid email.'
            )
        ),
        'image_location' => array(
            'mimeType' => array(
                'rule' => array('mimeType', array( 'image/png', 'image/jpg', 'image/jpeg')),
                'message' =>  'Please upload images only (png, jpg).'
            ),
            'fileSize' => array(
                'rule' => array('fileSize', '<=', '5MB'),
                'message' => 'Image must be less than 5MB.'
            )
        )
    );

    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        if (isset($this->data[$this->alias]['image_location'])) {
            $tmp = $this->data[$this->alias]['image_location']['tmp_name'];
            $hash = rand();
            $date = date("Ymd");
            $image = $date.$hash;
            $target = WWW_ROOT.'img'.DS.'uploads'.DS;
            $target = $target.basename($image);
            $image_location = "uploads/".$image;
            $this->data[$this->alias]['image_location'] = $image_location;
            move_uploaded_file($tmp, $target);
        }
        return true;
    }
}
