<?php
class Repost extends AppModel
{
    public $belongsTo = array('User','Post');
    public $actsAs = array('CakeSoftDelete.SoftDeletable');
    public $validate = array(
        'post' => array(
            'rule' => 'notBlank'
        ),
        'post_id' => array(
            'rule' => 'numeric'
        ),
        'user_id' => array(
            'rule' => 'numeric'
        )
    );

    // public function isOwnedBy($post, $user) {
    //     return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    // }
}
