<?php

class Follower extends AppModel
{
    public $validate = array(
        'follower_user_id' => array(
            'rule' => 'numeric'
        ),
        'following_user_id' => array(
            'rule' => 'numeric'
        )
    );
}
