<?php

class Comment extends AppModel
{
    public $belongsTo = array('User');
    public $actsAs = array('CakeSoftDelete.SoftDeletable');
    public $validate = array(
        'comment' => array(
            'rule' => 'notBlank'
        ),
        'user_id' => array(
            'rule' => 'numeric'
        ),
        'post_id' => array(
            'rule' => 'numeric'
        )
    );
}
