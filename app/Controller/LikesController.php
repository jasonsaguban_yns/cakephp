<?php

class LikesController extends AppController
{
    public $helpers = array('Html', 'Form');
    
    public function add($postId, $userId)
    {
        $this->request->onlyAllow('get');
        $this->autoRender=false;
        $like = array(
            'post_id' => $postId,
            'user_id' => $userId
        );
        $check = $this->Like->find('all',array(
            'conditions' => array(
                'Like.post_id' => $postId,
                'Like.user_id' => $userId
            )
        ));
        if (!empty($check)) {
            echo json_encode(array('status' => 'liked'));
            exit;
        }
        $this->Like->create();
        if ($this->Like->save($like)) {
            echo json_encode(
                array(
                    'post_id' => $postId,
                    'user_id' => $userId,
                    'status' => 'liked'
                )
            );
        } else {
            echo json_encode(
                array(
                    'post_id' => $postId,
                    'user_id' => $userId,
                    'status' => 'error'
                )
            );
        }
    }

    public function delete($postId, $userId)
    {
        $this->request->onlyAllow('get');
        $this->autoRender=false;
        $like = array(
            'post_id' => $postId,
            'user_id' => $userId
        );

        if ($this->Like->deleteAll(array('Like.post_id' => $postId , 'Like.user_id' => $userId))) {
            echo json_encode(
                array(
                    'post_id' => $postId,
                    'user_id' => $userId,
                    'status' => 'like'
                )
            );
        } else {
            echo json_encode(
                array(
                    'post_id' => $postId,
                    'user_id' => $userId,
                    'status' => 'error'
                )
            );
        }
    }
}
