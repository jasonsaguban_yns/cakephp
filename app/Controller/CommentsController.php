<?php

class CommentsController extends AppController
{
    public $helpers = array('Html', 'Form');

    public function add()
    {
        if ($this->request->is('post')) {
            $this->Comment->create();
            $postId = $this->request->data['post_id'];
            if ($this->Comment->save($this->request->data)) {
                $_SESSION['alert_message'] = 'Comment added successfully!';
                return $this->redirect(array('controller' => 'Posts','action' => 'view',$postId));
            }
        }
    }

    public function edit($id = null)
    {
        $this->request->onlyAllow('post');
        $this->autoRender=false;
        $comment = $_POST['comment'];
        $data = array(
            'comment' => $comment
        );
        
        $this->Comment->id = $id;
        if ($this->Comment->save($data)) {
            echo json_encode(
                array(
                    'status' => 'saved'
                )
            );
        } else {
            echo json_encode(
                array(
                    'status' => 'error'
                )
            );
        }
    }

    public function delete($id = null)
    {
        $this->request->onlyAllow('get');
        $this->autoRender=false;

        if ($this->Comment->delete($id)) {
            echo json_encode(
                array(
                    'status' => 'deleted'
                )
            );
        } else {
            echo json_encode(
                array(
                    'status' => 'error'
                )
            );
        }
    }
}
