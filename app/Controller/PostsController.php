<?php

class PostsController extends AppController
{
    public $helpers = array('Html', 'Form','Paginator');
    public $components = array(
        'Paginator'
      );

    public function index()
    {
        $this->loadModel('Follower');
        $userId = $this->Auth->user('id');
        $this->paginate = array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = Post.user_id'
                        )
                    ),
                array(
                    'table' => 'reposts',
                    'alias' => 'Reposts',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Reposts.id = Post.repost_id'
                        )
                    ),
                array(
                    'table' => 'posts',
                    'alias' => 'Post_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Post_2.id = Reposts.post_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'User_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User_2.id = Post_2.user_id'
                    )
                )
            ),
            'fields' => array(
                "Post.id",
                "Post.user_id",
                "Post.post",
                "Post.repost_id",
                "Post.created",
                "Post.modified",
                'Post.deleted',
                "User.id",
                "User.first_name",
                "User.last_name",
                "User.username",
                "User.image_location",
                "Reposts.id",
                "Reposts.post_id",
                "Post_2.id",
                "Post_2.post",
                "Post_2.created",
                "Post_2.deleted",
                "User_2.id",
                "User_2.first_name",
                "User_2.last_name",
                "User_2.username",
                "User_2.image_location"
            ),
            'conditions' => array(
                'Post.deleted IS NULL',
                'Post.user_id IN (SELECT following_user_id from followers WHERE follower_user_id = '.$userId.') OR Post.user_id = '.$userId,
                
            ),
            'order' => 'Post.created desc',
            'limit' => '10'
        );
        $posts = $this->paginate('Post');
        $this->set('posts', $posts);
        
        $followings = $this->Follower->find('list', array(
            'conditions' => array('follower_user_id' => $userId),
            'fields' => 'following_user_id'
        ));
        $followings= array_values($followings);
        $this->set('following', $followings);

    }

    public function view($id = null)
    {
        $post = $this->Post->find('first', array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = Post.user_id'
                        )
                ),
                array(
                    'table' => 'reposts',
                    'alias' => 'Reposts',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Reposts.id = Post.repost_id'
                        )
                    ),
                array(
                    'table' => 'posts',
                    'alias' => 'Post_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Post_2.id = Reposts.post_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'User_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User_2.id = Post_2.user_id'
                    )
                )
            ),
            'fields' => array(
                "Post.id",
                "Post.user_id",
                "Post.post",
                "Post.repost_id",
                "Post.created",
                "Post.modified",
                "User.id",
                "User.first_name",
                "User.last_name",
                "User.username",
                "User.image_location",
                "Reposts.id",
                "Reposts.post_id",
                "Post_2.id",
                "Post_2.post",
                "Post_2.created",
                "Post_2.deleted",
                "User_2.id",
                "User_2.first_name",
                "User_2.last_name",
                "User_2.username",
                "User_2.image_location"
            ),
            'conditions' => array("Post.id" => $id)

        ));
        if (empty($post)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->loadModel('Comment');
        $this->paginate = array(
            'limit' => 10,
            'order' => array('Comment.created' => 'asc'),
            'conditions' => array('Comment.post_id' => $id)
        );
        $comments = $this->paginate('Comment');
        $userId = $this->Auth->user('id');
        $this->loadModel('Follower');
        $followings = $this->Follower->find('list', array(
            'conditions' => array('follower_user_id' => $userId),
            'fields' => 'following_user_id'
        ));
        $followings= array_values($followings);
        $this->set('following', $followings);
        $this->set('post', $post);
        $this->set('comments', $comments);
    }

    public function add()
    {
        $this->autoRender=false;
        if ($this->request->is('post')) {
            $this->Post->create();
            $userId = $this->Auth->user('id');
            $post = $_POST['post'];
            $data = array (
                'post' => $post,
                'user_id' => $userId
            );
            if ($this->Post->save($data)) {
                $addedPost = $this->Post->findById($this->Post->getLastInsertID());
                echo json_encode(
                    array(
                        'id' => $addedPost['Post']['id'],
                        'user_id' => $addedPost['Post']['user_id'],
                        'post' => $addedPost['Post']['post'],
                        'repost_id' => $addedPost['Post']['repost_id'],
                        'created' => $addedPost['Post']['created']
                    )
                );
            }
        }
    }

    public function repost($id, $userId)
    {
        $this->request->onlyAllow('post');
        $this->autoRender=false;
        $dataForRepost = array(
            'post_id' => $id,
            'user_id' => $userId
        );
        $post = $_POST['post'];
        $this->loadModel('Repost');
        if ($this->Repost->save($dataForRepost)) {
            $repostId = $this->Repost->getLastInsertID();
            if (!empty($post)) {
                $dataForPost = array(
                    'user_id' => $userId,
                    'post' => $post,
                    'repost_id' => $repostId
                );
            } else {
                $dataForPost = array(
                    'user_id' => $userId,
                    'repost_id' => $repostId
                );
            }

            if ($this->Post->save($dataForPost)) {
                echo json_encode(
                    array(
                        'status' => 'reposted'
                    )
                );
            }
        }
    }

    public function deleteRepost($id)
    {
        $this->autoRender=false;
        $this->loadModel('Repost');
        if ($this->Repost->delete($id)) {
            return true;
        }
    }

    public function edit($id = null)
    {
        $this->autoRender=false;
        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            $data = array(
                'id' => $id,
                'post' => $_POST['post']
            );
            if ($this->Post->save($data)) {
                echo json_encode(
                    array(
                        'status' => 'edited'
                    )
                );
            }
        }
    }

    public function delete($id, $repostId = null)
    {
        $this->autoRender=false;
        if ($this->Post->delete($id)) {
            $this->deleteRepost($repostId);
            echo json_encode(
                array(
                    'status' => 'deleted'
                )
            );
        }

        if (!empty($_GET['fromview'])) {
            $_SESSION['alert_message'] = 'Post deleted successfully!';
            return $this->redirect(array('controller' => 'posts','action' => 'index'));
        }
    }
}
