<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController
{

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register and logout.
        $this->Auth->allow('add', 'logout', 'verify');
    }

    public function search($data)
    {
        $this->request->allowMethod('get');
        $data = trim($data);
        $this->paginate = array(
            'conditions' => array(
                "concat_ws(' ',first_name,last_name,username) LIKE '%".$data."%'"
            ),
            'fields' => array(
                'id',
                'first_name',
                'last_name',
                'username',
                'image_location'
            ),
            'limit' => '10'
        );

        $results = $this->paginate('User');
        $this->set('results', $results);
        $this->Session->write('Searchtext', $data);
        $this->set('search', $data);

        $userId = $this->Auth->user('id');
        $this->loadModel('Follower');
        $followingList = $this->Follower->find('list', array(
            'conditions' => array('follower_user_id' => $userId),
            'fields' => 'following_user_id'
        ));
        $followingList = array_values($followingList);
        $this->set('following', $followingList);
    }

    public function index()
    {
        $this->autoRender = false;
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null)
    {
        $this->User->id = $id;
        $userId = $this->Auth->user('id');

        $user = $this->User->find('first', array(
            'conditions' => array('User.id' => $id),
            'fields' => array(
                'User.id',
                'User.first_name',
                'User.last_name',
                'User.username',
                'User.created',
                'User.image_location'
            )
        ));
        if (empty($user)) {
            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
        $this->set('user', $user);

        $this->loadModel('Post');
        $this->paginate = array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = Post.user_id'
                        )
                    ),
                array(
                    'table' => 'reposts',
                    'alias' => 'Reposts',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Reposts.id = Post.repost_id'
                        )
                    ),
                array(
                    'table' => 'posts',
                    'alias' => 'Post_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Post_2.id = Reposts.post_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'User_2',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User_2.id = Post_2.user_id'
                    )
                )
            ),
            'fields' => array(
                "Post.id",
                "Post.user_id",
                "Post.post",
                "Post.repost_id",
                "Post.created",
                "Post.modified",
                "User.id",
                "User.first_name",
                "User.last_name",
                "User.username",
                "User.image_location",
                "Reposts.id",
                "Reposts.post_id",
                "Post_2.id",
                "Post_2.post",
                "Post_2.created",
                "Post_2.deleted",
                "User_2.id",
                "User_2.first_name",
                "User_2.last_name",
                "User_2.username",
                "User_2.image_location"
            ),
            'conditions' => array('Post.user_id' => $id),
            'order' => 'Post.created desc',
            'limit' => '10'
        );
        $posts = $this->paginate('Post');
        $this->set('posts', $posts);


        $this->loadModel('Follower');
        $followingList = $this->Follower->find('list', array(
            'conditions' => array('follower_user_id' => $userId),
            'fields' => 'following_user_id'
        ));
        $followingList= array_values($followingList);
        $this->set('following', $followingList);

        $noOfFollowers = $this->Follower->find('count', array(
            'conditions' => array('following_user_id' => $id),
        ));
        $this->set('noOfFollowers', $noOfFollowers);

        $noOfFollowing = $this->Follower->find('count', array(
            'conditions' => array('follower_user_id' => $id),
        ));
        $this->set('noOfFollowing', $noOfFollowing);

        $noOfPost = $this->Post->find('count', array(
            'conditions' => array('user_id' => $id),
        ));
        $this->set('noOfPost', $noOfPost);

        $followers = $this->Follower->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = Follower.Follower_user_id'
                        )
                )
            ),
            'conditions' => array (
                'Follower.following_user_id' => $id
            ),
            'fields' => array(
                'User.id',
                'User.first_name',
                'User.last_name',
                'User.username',
                'User.image_location'
            )
        ));
        $this->set('followers', $followers);

        $followings = $this->Follower->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'User.id = Follower.Following_user_id'
                        )
                )
            ),
            'conditions' => array (
                'Follower.follower_user_id' => $id
            ),
            'fields' => array(
                'User.id',
                'User.first_name',
                'User.last_name',
                'User.username',
                'User.image_location'
            )
        ));
        $this->set('followings', $followings);
    }

    public function add()
    {
        if ($this->request->is('post')) {
            $this->User->create();            
            $frmData = $this->request->data;
            $token= bin2hex(random_bytes(16));
            $frmData["User"]["token"] = $token;
            $frmData["User"]["status"] = 0;
            $frmData['User']['username'] = trim($frmData['User']['username']);
            $frmData['User']['first_name'] = trim($frmData['User']['first_name']);
            $frmData['User']['last_name'] = trim($frmData['User']['last_name']);
            if ($this->User->save($frmData)) {
                $baseUrl = Router::url('/', true);
                $Email = new CakeEmail();
                $Email->config('gmail');
                $id = $this->User->getLastInsertID();
                $ms = 'Click on the link below to complete registration ';
                $ms.= $baseUrl.'users/verify/t:'.$token.'/n:'.$id.'';
                $ms = wordwrap($ms, 70);
                $Email->from('microblog.yns@gmail.com')
                    ->to($frmData['User']['email'])
                    ->subject('Confirm Registration for MicroBlog.')
                    ->send($ms);
                $this->Flash->success(__('An activation link has been sent to your email,kindly check your inbox or spams.'));
                return $this->redirect(array('action' => 'login'));
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
    }

    public function edit($id = null)
    {
        $userid = $this->Auth->user('id');
        if ($id == $userid) {
            //Edit view
            $this->User->id = $id;
            $user = $this->User->find('first', array(
                'fields' => array(
                    'User.id',
                    'User.first_name',
                    'User.last_name',
                    'User.username',
                    'User.email',
                    'image_location'
                ),
                'conditions' => array('User.id' => $id)
            ));
            $this->set('user', $user);

            if ($this->request->is('post') || $this->request->is('put')) {
                //Editing basic user information
                if (empty($this->request->data['User']['current-pass'])) {
                    if ($this->User->save($this->request->data)) {
                        $this->Session->write('Auth', $this->User->read(null, $userid));
                        $this->Flash->success(__('Updated basic user informations.'));
                        return $this->redirect(array('action' => 'edit', $userid));
                    } else {
                        return $this->Flash->error(__('You have some error, please try again.'));
                    }
                } else { //Editing user password
                    if ($this->request->data['User']['new-pass-1'] !==  $this->request->data['User']['new-pass-2']) {
                        return $this->Flash->error(__('New password is mismatch.'));
                    }
                    $user = $this->User->findById($userid);
                    if ($this->checkPassword($this->request->data['User']['current-pass'], $user['User']['password'])) {
                        $data = array('password' => $this->request->data['User']['new-pass-1']);
                        if ($this->User->save($data)) {
                            $this->Session->write('Auth', $this->User->read(null, $userid));
                            $this->Flash->success(__('Password successfully updated.'));
                            return $this->redirect(array('action' => 'edit', $userid));
                        } else {
                            return $this->Flash->error(__('Error in saving your new password.'));
                        }
                    } else {
                        return $this->Flash->error(__('You entered a wrong current password.'));
                    }
                }
            }
        } else {
            return $this->redirect(array('controller' => 'posts','action' => 'index'));
        }
    }

    public function checkPassword($inputPassword, $currentPass)
    {
        return (new BlowfishPasswordHasher)->check($inputPassword, $currentPass);
    }

    public function login()
    {
        if (!empty($this->Auth->User('id'))) {
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                if ($this->Auth->user('status') == 0) {
                    $this->Flash->error("Your account is inactive,check your email to activate your account!");
                    return $this->redirect(['action'=>'logout']);
                } else {
                    return $this->redirect($this->Auth->redirectUrl(['controller' => 'posts', 'action' => 'index']));
                }
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
    
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    function verify()
    {
        if (!empty($this->passedArgs['n']) && !empty($this->passedArgs['t'])) {
            $id = $this->passedArgs['n'];
            $token = $this->passedArgs['t'];
            $result = $this->User->find('first', array(
                'conditions' => array('id' => $id),
                'fields' => array('status','token','id')
            ));
            $this->User->id = $id;
            if (empty($result)) {
                $this->Flash->error("Account is invalid, kindly create another one.");
                $this->redirect($this->Auth->redirectUrl(['controller' => 'users', 'action' => 'add']));
            }
            if ($result['User']['status'] == 0) {
                if ($result['User']['token'] == $token) {
                    $result['User']['status'] = 1;
                    if ($this->User->save($result)) {
                        $this->Flash->success(__('Your account have been successfully activated.'));
                        $this->redirect($this->Auth->redirectUrl(['controller' => 'users', 'action' => 'login']));
                    } else {
                        $this->Flash->error("Your registration failed please try again");
                        $this->redirect($this->Auth->redirectUrl(['controller' => 'users', 'action' => 'add']));
                    }
                } else {
                    $this->Flash->error("Your registration failed please try again");
                    $this->redirect($this->Auth->redirectUrl(['controller' => 'users', 'action' => 'add']));
                }
            } else {
                $this->Flash->success(__('Your account is already activated.'));
                $this->redirect($this->Auth->redirectUrl(['controller' => 'users', 'action' => 'login']));
            }
        }
    }
}
