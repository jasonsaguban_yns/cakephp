<?php
class AppController extends Controller {
    //...

    public $components = array(
        'Flash',
        'Auth' => array(
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        ),
        'Session'                
    );

    public function beforeFilter() {
        // $this->Auth->allow('index', 'view');
    }
    
}
