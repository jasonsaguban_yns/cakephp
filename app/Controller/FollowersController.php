<?php

class FollowersController extends AppController
{
    public $helpers = array('Html', 'Form');
    
    public function add($followingId)
    {
        $this->request->onlyAllow('get');
        $this->autoRender=false;
        $data = array(
            'follower_user_id' => $this->Auth->user('id'),
            'following_user_id' => $followingId
        );
        $check = $this->Follower->find('first',array(
            'conditions' => array(
                'follower_user_id' => $this->Auth->user('id'),
                'following_user_id' => $followingId
            )
        ));
        if (!empty($check)) {
            echo json_encode(array('status' => 'followed'));
            exit;
        }
        $this->Follower->create();
        if ($this->Follower->save($data)) {
            echo json_encode(array('status' => 'followed'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function delete($followingId)
    {
        $this->request->onlyAllow('get');
        $this->autoRender=false;
        $followerUserId = $this->Auth->user('id');
        $data = array(
            'Follower.follower_user_id' => $followerUserId,
            'Follower.following_user_id' => $followingId
        );

        if ($this->Follower->deleteAll($data)) {
            echo json_encode(array('status' => 'unfollowed'));
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }
}
