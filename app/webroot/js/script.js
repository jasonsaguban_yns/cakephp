var hostname = window.location.origin;
//loader
$(window).on('load',function(){
    $("div.loader-wrapper").hide("fade");
    $("#content").css("display","block");
});
//flash message
if ($("#alert-message").text().length !== 0) {
    $("#myAlert").show('fade');
    setTimeout(function () {
        $("#myAlert").hide('fade');
    },2000);
    $("#alertClose").click(function(){
        $("#myAlert").hide('fade');
    })
}
setTimeout(function () {
    $("#flashMessage").hide('fade');
},8000);

var maxLen = 140;
// Adding post function
$("#addPostBtn").click(function(e){
    e.preventDefault();
    var textarea = $("#addPostTextArea");
    var post = textarea.val().trim();
    if (post.length !== 0 ) {
        var data = `post=${post}`;
        var xhr = new XMLHttpRequest;
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                showAlert("Post added successfully!");
                $("#addPostTextArea").val('');
            } catch (e) {
                console.log('Could not parse JSON!');
                redAlert("Error in adding post.");
            }
        }
        xhr.open("POST" , hostname + "/posts/add", true);
        xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
        xhr.send(data);
    }
})

// Editing post function
$(".dropdown-item.edit-post").click(function(e){
    e.preventDefault();
    var postId = e.target.id;
    var editDiv = $("div#edit-post" + postId);
    var postDiv = $("p#post-" + postId);
    var postText = postDiv.text();    
    var textArea = $("#textarea-" + postId);
    var postActions = $("div.post-actions")
    var cancelBtn = $("#cancelBtn-" + postId);
    var saveChangesBtn = $("#button-" + postId);
    editDiv.css("display","block");
    postDiv.css("display","none");
    cancelBtn.css("display","block");
    postActions.css("display","none");
    textArea.focus();
    textArea.text(postText);
    charsLeft();

    cancelBtn.click(function(e){
        postActions.css("display","block");
        postDiv.css("display","block");
        editDiv.css("display","none");  
        cancelBtn.css("display","none");   
        textArea.val(postText);     
    })
    
    textArea.keydown(charsLeft);
    function charsLeft() {
        var remainingLen = maxLen - (textArea.val()).length;
        var count = $("#charCounter-" + postId);
        count.text(remainingLen + " characters left.")  ;
    }
    
    saveChangesBtn.click(function(e){
        var textAreaText = textArea.val().trim();
        if (textAreaText.length !== 0) {
            var xhr = new XMLHttpRequest;
            xhr.onload = () => {
                let response = null;
                try {
                    response = JSON.parse(xhr.responseText);
                postDiv.text(textAreaText);
                    postActions.css("display","block");
                    postDiv.css("display","block");
                    editDiv.css("display","none");
                    cancelBtn.css("display","none");
                    showAlert("Post edited successfully!")
                } catch (e) {
                    console.error('Could not parse JSON!');
                    redAlert('Error in editing your post.');
                }
            }
            var data = `post=${textAreaText}`;
            xhr.open("POST", hostname +"/posts/edit/"+postId, true);
            xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
            xhr.send(data);
        }
    });
    
})

// Deleting post function
$(".dropdown-item.delete-post").click(function(e){
    e.preventDefault();
    var postId = e.target.id;
    var repostId = e.target.name
    var cardPost = $("div#card-post-"+postId)
    var xhr = new XMLHttpRequest;
    xhr.onload = () => {
        let response = null;
        try {
            response = JSON.parse(xhr.responseText);
            cardPost.css("display","none");
            showAlert("Post deleted successfully!");            
        } catch (event) {
            console.error('Could not parse JSON!');
            redAlert('Error in deleting your post.');
        }
    }
    xhr.open("GET", hostname + "/posts/delete/"+postId+"/"+repostId, true);
    xhr.send();

});

// like function
$("a.float-right.mr-2.mb-2.like").click(function(e){
    e.preventDefault(); 
    var postId =e.target.name;
    var userId =e.target.id;
    const xhr = new XMLHttpRequest;
    var noOfLikes = $("#noOfLikes" + postId);
    var currentNoOfLikes = Number(noOfLikes.text());
    //if the post is already liked, and the user want to unlike the post
    if (e.target.innerHTML == "Liked") {        
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'like') {
                    e.target.innerHTML = "Like";
                    noOfLikes.text( currentNoOfLikes - 1);
                } 
            } catch (e) {
                console.error('Could not parse JSON!');
            }
        };
        xhr.open("GET", hostname + "/likes/delete/"+postId+"/"+userId, true);
        xhr.send();
    } else { //if the user want to like the post        
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'liked') {
                    e.target.innerHTML = "Liked";
                    noOfLikes.text( currentNoOfLikes + 1);
                } 
            } catch (e) {
                console.error('Could not parse JSON!');
                console.log(xhr.responseText);
            }
        };
        xhr.open("GET", hostname + "/likes/add/"+postId+"/"+userId, true);
        xhr.send();
        // const data = `post_id=${postId}&user_id=${userId}`;
        // xhr.open('post' ,'/cakephp/likes/add',true);
        // xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
        // xhr.send(data);
    }
});

//Edit comment function
$("a.dropdown-item.edit").click(function(e){
    e.preventDefault();
    var commentId = e.target.id;
    var comment = $("p.card-text" + commentId);
    var commentText = comment.text();
    var textArea = $("#textarea-" + commentId);
    var commentActions = $(".commentActions");
    var editCommentDiv = $("div.edit-comment" + commentId);
    var cancelBtn = $("#cancelBtn-" + commentId);
    var saveChangesBtn = $("#button-" + commentId);

    textArea.text(commentText);
    commentActions.css("display","none");
    comment.css("display","none");
    editCommentDiv.css("display","block");   
    cancelBtn.css("display","block");
    textArea.focus();
    charsLeft();

    cancelBtn.click(function(e){
        commentActions.css("display","block");
        comment.css("display","block");
        editCommentDiv.css("display","none");  
        cancelBtn.css("display","none");   
        textArea.val(commentText);     
    })

    textArea.keydown(charsLeft);
    function charsLeft() {
        var remainingLen = maxLen - (textArea.val()).length;
        var count = $("#charCounter-" + commentId);
        count.text(remainingLen + " characters left.")  ;
    }

    saveChangesBtn.click(function(e){
        var textAreaText = textArea.val().trim();
        if (textAreaText.length !== 0) {
            var xhr = new XMLHttpRequest;
            xhr.onload = () => {
                let response = null;
                try {
                    response = JSON.parse(xhr.responseText);
                    if (response.status == "saved") {
                        comment.text(textAreaText);
                        commentActions.css("display","block");
                        comment.css("display","block");
                        editCommentDiv.css("display","none");
                        cancelBtn.css("display","none");
                        showAlert("Edited comment successfully!")
                    } else {
                        console.log('saving error');
                    }
                } catch (e) {
                    console.error('Could not parse JSON!');
                    redAlert('Error in editing your comment.');
                }
            }
            var data = `comment=${textAreaText}`;            
            xhr.open("POST", hostname + "/comments/edit/"+commentId, true);
            xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
            xhr.send(data);
        }
    });
});

//Delete comment function
$("a.dropdown-item.delete").click(function(e){
    e.preventDefault();
    var noOfComments = Number($("#noOfComments").text());
    var commentId = e.target.id;
    var xhr = new XMLHttpRequest;
    xhr.onload = () => {
        let response = null;
        try {
            response = JSON.parse(xhr.responseText);
            if (response.status == "deleted") {
                $("#commentDiv-" + commentId).css("display" ,"none");
                $("#noOfComments").text(noOfComments - 1);
                showAlert("Deleted comment successfully!");
            } else{
                console.log("delete error");
            }
        } catch (event) {
            console.error('Could not parse JSON!');
            redAlert('Error in deleting your comment');
        }
    }
    xhr.open("GET", hostname + "/comments/delete/"+commentId, true);
    xhr.send();
});

//Retweet post function
$("button.btn.btn-primary.repost").click(function(e){
    e.preventDefault();
    var postId = e.target.name;
    var userId = e.target.id;
    var post = $("textarea#repost-" + postId).val();
    var data = `post=${post}`;
    var xhr = new XMLHttpRequest;
    var noOfReposts = $("#noOfReposts" + postId);
    var currentNoOfReposts = Number(noOfReposts.text());

    xhr.onload = () => {
        response = null;
        try {
            response = JSON.parse(xhr.responseText);
            $("#repostModal-" + postId).modal("hide");
            noOfReposts.text(currentNoOfReposts + 1);
            showAlert("Repost Successfully!");

        } catch (e) {
            console.log('Could not parse JSON!');
            redAlert('Error in reposting a post.');
        }
    }

    xhr.open("POST" , hostname + "/posts/repost/" + postId + "/" + userId, true);
    xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
    xhr.send(data);
})

// Showing profile picture function
var uploadedImage = $("#image-btn");
var currentImage = $("#current-image");
uploadedImage.on('change', function(e) {
    const file = this.files[0];

    if (file) {
        const reader = new FileReader();
        reader.addEventListener("load",function(){
            currentImage.attr("src" ,this.result);
        })
        reader.readAsDataURL(file);
    } else {

    }
})

//Following Unfollowing function
$(".follow-button").click(function(e){    
    e.preventDefault();
    var followingId = e.target.id;
    const xhr = new XMLHttpRequest;
    //unfollowing
    if (e.target.innerHTML == "Following") {        
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'unfollowed') {
                    e.target.innerHTML = "Follow";
                    showAlert("User uccessfully unfollowed.")
                }
            } catch (e) {
                console.error('Could not parse JSON!');
                redAlert('Error in unfollowing a user.');
            }
        };
        xhr.open("GET", hostname + "/followers/delete/"+followingId, true);
        xhr.send();
    } else if (e.target.innerHTML == "Follow") { //following     
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'followed') {
                    e.target.innerHTML = "Following";
                    showAlert("User successfully followed.");
                } 
            } catch (e) {
                console.error('Could not parse JSON!');
                redAlert('Error in following a user.');
            }
        };
        xhr.open("GET", hostname + "/followers/add/"+followingId, true);
        xhr.send();
    }

})


$("#searchBtn").click(function(e){
    e.preventDefault();
    var searchKey = $("#searchInput").val().trim();
    if (searchKey.length !== 0 ) {
        window.location.href = hostname + "/users/search/" + searchKey ; 
    }    
})

$('#searchInput').bind("enterKey",function(e){
    var searchKey = $("#searchInput").val().trim();
    if (searchKey.length !== 0 ) {
        window.location.href = hostname + "/users/search/" + searchKey ; 
    }   
 });
$('#searchInput').keyup(function(e){
     if(e.keyCode == 13)
     {
         $(this).trigger("enterKey");
     }
 });

$("#showPostsBtn").click(function(e){
    e.preventDefault();
    $("#showPosts").css("display","block")
    $("#showFollowings").css("display","none")
    $("#showFollowers").css("display","none")
})

$("#showFollowingsBtn").click(function(e){
    e.preventDefault();
    $("#showPosts").css("display","none")
    $("#showFollowings").css("display","block")
    $("#showFollowers").css("display","none")
})

$("#showFollowersBtn").click(function(e){
    e.preventDefault();
    $("#showPosts").css("display","none")
    $("#showFollowings").css("display","none")
    $("#showFollowers").css("display","block")
})


function showAlert(message) {
    $("#alert-message").html(message);
    $("#myAlert").attr("class","alert alert-success collapse");
    $("#myAlert").show('fade');
    setTimeout(function () {
        $("#myAlert").hide('fade');
    },2000);
    $("#alertClose").click(function(){
        $("#myAlert").hide('fade');
    })
}

function redAlert(message) {
    $("#alert-message").html(message);
    $("#myAlert").attr("class","alert alert-danger collapse");
    $("#myAlert").show('fade');
    setTimeout(function () {
        $("#myAlert").hide('fade');
    },2000);
    $("#alertClose").click(function(){
        $("#myAlert").hide('fade');
    })
}

document.querySelector("textarea.mt-5").addEventListener("keydown",function(){
    var remainingLen = maxLen - (document.querySelector("textarea.mt-5").value).length;
    var count = document.getElementById('charCounter');
    count.innerHTML =  remainingLen + " characters left." ;
});



