<?= $this->Html->css('registers') ?>
<div class="text-center">
<?php echo $this->Flash->render('auth'); ?>
<?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
    <fieldset>
        <h1 class="h1 mb-3 font-weight-normal">
            <?php echo __('Login'); ?>
        </h1>
        <?php
            echo $this->Form->input(
                'username',
                array('class' => 'form-control', 'placeholder' => 'Username','label' => '' )
            );

            echo $this->Form->input(
                'password',
                array('class' => 'form-control', 'placeholder' => 'Password','label' => '' )
            );
            ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Sign in', array('class' => 'btn btn-success')) ?>
<?php echo $this->Form->end(); ?>
<?= $this->Html->link('No account yet? Register now!', array('action' => 'add')) ?>
</div>
