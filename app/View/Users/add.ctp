<?= $this->Html->css('registers') ?>
<div class="text-center">
<h1 class="h1 mb-3 font-weight-normal">Register</h1>
<?php echo $this->Form->create('User', array('type'=>'file','class' => 'form-signin')); ?>
    <fieldset>
        <?php echo $this->Form->input(
            'username',
            array('class' => 'form-control my-2', 'placeholder' => 'Username','label' => 'Username' )
        );
        echo $this->Form->input(
            'password',
            array('class' => 'form-control', 'placeholder' => 'Password','label' => 'Password' )
        );

        echo $this->Form->input(
            'email',
            array('class' => 'form-control my-2', 'placeholder' => 'Email','label' => 'Email' )
        );

        echo $this->Form->input(
            'first_name',
            array('class' => 'form-control my-2', 'placeholder' => 'First Name','label' => 'First Name' )
        );

        echo $this->Form->input(
            'last_name',
            array('class' => 'form-control my-2', 'placeholder' => 'Last Name','label' => 'Last Name' )
        );
        echo '<br>';

        echo $this->Form->input(
            'image_location',
            array('class' => 'form-control my-2', 'label' => 'Profile Picture','type'=>'file' )
        );

        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Sign Up', array('class' => 'btn btn-success')) ?>
<?php echo $this->Form->end() ?>
<?= $this->Html->link('Already have an account?Login now!', array('action' => 'login')) ?>
</div>
