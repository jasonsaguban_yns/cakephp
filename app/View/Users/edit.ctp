<?php
    $userImage = $user['User']['image_location'];
    $firstName = ucfirst($user['User']['first_name']);
    $lastName = ucfirst($user['User']['last_name']);
    $username = $user['User']['username'];
    $email = $user['User']['email'];
    $userId = $user['User']['id'];
    unset($user);
?>
<div class="card border-primary my-4 mx-auto " style="max-width: 35rem;">
    <!-- User profile picture -->
    <div class="card-header">    
        <?= $this->Html->image($userImage, array('id' => 'current-image','width' => '200','height' => '200','class' => 'frounded mx-auto d-block')) ?>
        <?= $this->Form->create('User', array('type'=>'file','id' => 'userProfilePicture')); ?>
        <?= $this->Form->input(
            'image_location',
            array('class' => 'mt-2 mx-auto', 'label' => false,'type'=>'file','id' => 'image-btn' )
        ) ?>
        <?= $this->Form->button('Save Changes', array('class' => 'btn btn-outline-primary btn-sm float-right my-2','id' => 'image-save-btn','type' => 'submit')) ?>
        <?= $this->Form->end() ?>
    </div>
    <div class="card-body">
        <div class="accordion" id="accordionExample">
            <!-- Basic user informations -->
            <div class="card">
                <div class="card-header p-0" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-sm btn-outline-primary btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Basic User Informations
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <?= $this->Form->create('User', array('id' =>'basicUserInfo')) ?>
                        <div class="input-group mx-auto mb-3 col-9">
                            <span class="input-group-text" id="basic-addon1">@</span>
                            <?= $this->Form->input(
                                'username',
                                array('class' => 'form-control', 'placeholder' => 'Username','label' => false, 'value' => $username )
                            ) ?>
                            <input type="hidden" name="userId" id='user-id' value=<?= $userId?>>
                        </div>
                        <div class="input-group mx-auto mb-3 col-9">
                            <span class="input-group-text">First name</span>
                            <?= $this->Form->input(
                                'first_name',
                                array('class' => 'form-control', 'placeholder' => 'First Name','label' => false, 'value' => $firstName )
                            ) ?>
                        </div>
                        <div class="input-group mx-auto mb-3 col-9">
                            <span class="input-group-text">Last name</span>
                            <?= $this->Form->input(
                                'last_name',
                                array('class' => 'form-control', 'placeholder' => 'Last Name','label' => false, 'value' => $lastName )
                            ) ?>
                        </div>
                        <div class="input-group mx-auto mb-3 col-9">
                            <span class="input-group-text">Email</span>
                            <?= $this->Form->input(
                                'email',
                                array('class' => 'form-control', 'placeholder' => 'Email','label' => false, 'value' => $email )
                            ) ?>
                        </div>
                        <?= $this->Form->button('Save Changes', array('class' => 'btn btn-outline-primary btn-sm float-right mb-3','id' => 'basic-info-btn','type' => 'submit')) ?>
                    </div>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- Password changer -->
            <div class="card">
                <div class="card-header p-0" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-sm btn-outline-primary btn-block text-left collapsed p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        User Password Settings
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                    <?= $this->Form->create('User', array('id' =>'userPasswordEdit')) ?>
                        <div class="input-group mx-auto mb-3 col-12">
                            <span class="input-group-text" id="basic-addon1">Current Password</span>
                            <?= $this->Form->input(
                                'current-pass',
                                array('class' => 'form-control', 'placeholder' => 'Current Password','label' => false,'type' => 'password')
                            )?>
                        </div>
                        <div class="input-group mx-auto mb-3 col-12">
                            <span class="input-group-text">New Password</span>
                            <?= $this->Form->input(
                                'new-pass-1',
                                array('class' => 'form-control', 'placeholder' => 'New Password','label' => false, 'minLength' => '8','type' => 'password')
                            )?>
                        </div>
                        <div class="input-group mx-auto mb-3 col-12">
                            <span class="input-group-text">Re-type New Password</span>
                            <?= $this->Form->input(
                                'new-pass-2',
                                array('class' => 'form-control', 'placeholder' => 'Re-type New Password','label' => false, 'minLength' => '8','type' => 'password')
                            )?>
                        </div>
                        <?= $this->Form->button('Save Changes', array('class' => 'btn btn-outline-primary btn-sm float-right mb-3','id' => 'basic-info-btn','type' => 'submit')) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
