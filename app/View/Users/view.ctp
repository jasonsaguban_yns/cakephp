<?php $paginator = $this->Paginator; ?>

<div class="card border-primary my-4 mx-auto" style="max-width: 35rem;">
    <div class="card-header">
        <?php if ($this->Session->read('Auth.User.id') !== $user['User']['id']) : ?>
            <?php (in_array($user['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
        <button class="btn btn-outline-primary btn-sm float-right follow-button" style='display: inline-block;' id=<?= $user['User']['id'] ?>><?= $follow ?></button><br>
        <?php endif ?>
        <?= $this->Html->image($user['User']['image_location'], array('width' => '130','height' => '130','class' => 'float-left mr-1 mt-2')) ?>
        <strong style="font-size: 30px;display: inline-block;"><?= ucfirst($user['User']['first_name'])." ".ucfirst($user['User']['last_name']) ?></strong><br>
        <h6 class="card-subtitle text-muted mt-1">@<?= $user['User']['username'] ?></h6>
        <p class="card-text mt-4"><small class="text-muted">Joined MicroBlog since: <br> <?= date('F j, Y', strtotime($user['User']['created'])) ?></small></p>        
        <span class="float-right "><a id='showFollowingsBtn' href="">Following: <strong><?= $noOfFollowing ?></strong></a></span>
        <span class="float-right mr-3"><a id='showFollowersBtn' href="">Followers: <strong><?= $noOfFollowers ?></strong></a></span>
        <span class="float-right mr-3"><a id='showPostsBtn' href="">Posts: <strong><?= $noOfPost ?></strong></a></span>
    </div>
</div>
<hr class="bg-primary" style="max-width: 35rem; border-width: 5px">

<!-- Adding post section -->
<?php if ($user['User']['id'] == $this->Session->read('Auth.User.id')) : ?>
<div id='divAddPost' class="mx-auto" style="max-width: 35rem;">
    <textarea class="mt-5 col-10" id="addPostTextArea" cols="30" rows="5" required='required' maxLength = '140' placeholder="<?= "What's on your mind, ".ucfirst($this->Session->read('Auth.User.first_name'))."?"?>" ></textarea>
    <button id="addPostBtn" class="btn btn-primary float-right mt-5">Post</button>
    <br>   
    <small id="charCounter"></small>
</div>
<?php endif ?>

<!-- posts view section -->
<div id="showPosts">
<?php foreach ($posts as $key => $post) : ?>
    <?php
        $noOfComments = 0;
    for ($i = 0; $i < count($post['Comment']); $i++) {
        if ($post['Comment'][$i]['deleted'] == '') {
            $noOfComments++;
        }
    }
        $noOfRepost = 0;
    for ($i = 0; $i < count($post['Repost']); $i++) {
        if ($post['Repost'][$i]['deleted'] == '') {
            $noOfRepost++;
        }
    }
    ?>
    <div class="card border-primary my-4 mx-auto" id=<?= "card-post-".$post['Post']['id'] ?> style="max-width: 35rem;">
    
    <!-- header part of post section-->
    <div class="card-header ">
        <!-- actions to the post -->
        <button class="btn btn-sm btn-danger float-right" style="display: none;" id=<?= "cancelBtn-".$post['Post']['id']?>>Cancel</button>
        <div class="btn-group dropright float-right post-actions" >
            <button class="btn btn-primary dropdown-toggle dropdown-toggle-split btn-sm" data-toggle="dropdown">
            </button>
            <ul class="dropdown-menu">
            <?php if ($post['Post']['user_id'] === $this->Session->read('Auth.User.id')) : ?>
                <li>
                    <a href="#" class="dropdown-item delete-post" onclick="if (confirm('Are you sure you want to delete this post?') ) {return true;} else{ return false;}" id=<?=$post['Post']['id'] ?> name=<?=$post['Reposts']['id'] ?>>
                        Delete
                    </a>
                </li>
                <li>
                    <a href="#" class="dropdown-item edit-post" id=<?= $post['Post']['id'] ?>>Edit</a>
                </li>
            <?php endif ?>    
                <li>
                    <?= $this->Html->link(
                        'View Post',
                        array('controller' => 'posts' ,'action' => 'view', $post['Post']['id']),
                        array('class' => 'dropdown-item')
                    )?>
                </li>
            </ul>        
        </div>
        <!-- end of actions to the post -->

        <!-- post user informations -->
        <div>
            <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$post['User']['id'])) ?>>
            <?= $this->Html->image($post['User']['image_location'], array('width' => '60','height' => '60','class' => 'float-left mr-1 ')) ?>            
            <?= ucfirst($post['User']['first_name'])." ".ucfirst($post['User']['last_name']) ?>
            <br>            
            <h6 class="card-subtitle text-muted">@<?= $post['User']['username'] ?></h6>
            </a>
            <p class="card-text"><small class="text-muted"><?= date('F j, Y, g:i a', strtotime($post['Post']['created'])) ?></small></p>
        </div>
        <!-- end of post user information -->
    </div>
    <!-- end of header part of post section -->

    <!-- contents of the posts section -->
    <div class="card-body text-black">
        <p class= 'card-text mx-3' id=<?= 'post-'.$post['Post']['id'] ?> ><?= $post['Post']['post'] ?></p>
        <!-- editting of post section -->
        <div style="display: none;" class = "mb-5" id = <?= "edit-post".$post['Post']['id'] ?>>
            <textarea name="editPost" id=<?= "textarea-".$post['Post']['id'] ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
            <input type="hidden" name="postId" value=<?= $post['Post']['id'] ?>>
            <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$post['Post']['id'] ?>>Save Changes</button>
            <small class="ml-2" id=<?= "charCounter-".$post['Post']['id']?>></small>
        </div>
        <!-- end of editting of post section -->
        <!-- card for original post -->
        <?php if (!empty($post['Post_2']['id'])) : ?>
        <div class="card border-secondary">
            <div class='card-header'>
                <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$post['User_2']['id'])) ?>>
                <?= $this->Html->image($post['User_2']['image_location'], array('width' => '50','height' => '50','class' => 'float-left mr-1 ')) ?>
                <small>
                <?= ucfirst($post['User_2']['first_name'])." ".ucfirst($post['User']['last_name']) ?></small>
                <small class="card-subtitle text-muted">@<?= $post['User_2']['username'] ?></small><br>
                </a>
                <small class="text-muted"><?= date('F j, Y, g:i a', strtotime($post['Post_2']['created'])) ?></small>
                <?php if ($this->Session->read('Auth.User.id') !== $post['User_2']['id']) : ?>
                    <?php (in_array($post['User_2']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User_2']['id'] ?>><?= $follow ?></button><br>
                <?php endif ?>
            </div>
            <div class="card-body text-secondary">
            <small>
                <a href=<?= $this->Html->url(array('controller' => 'posts','action' => 'view',$post['Post_2']['id']))  ?> class="float-right"><?= (empty($post['Post_2']['deleted'])) ?  'View Original Post' : '' ?>
                </a>
            </small>
            <p class= 'card-text mt-3'><?= (empty($post['Post_2']['deleted'])) ?  $post['Post_2']['post'] :  '<strong><i>THIS POST WAS DELETED</i></strong>' ?></p>
            </div>
        </div>
        <?php endif ?>
        <!-- card for original post -->
    </div>
    <!-- end of contents of the posts section -->

    <!-- number of likes,comments,reposts section -->
    <div class="mx-3 text-secondary">
        <p class="card-text mx-1" style="display: inline">[Likes 
            <span id=<?= "noOfLikes".$post['Post']['id'] ?>><?= count($post['Like']) ?></span>]
        </p>
        <p class="card-text mx-1" style="display: inline">[Comments 
            <span id=<?= "noOfComments".$post['Post']['id'] ?>><?= $noOfComments ?></span>]
        </p>
        <p class="card-text mx-1" style="display: inline">[Reposts 
            <span id=<?= "noOfReposts".$post['Post']['id'] ?>><?= $noOfRepost ?></span>]
        </p>
    </div>
    <!-- number of likes,comments,reposts section -->
    <hr class="bg-primary" style="max-width: 35rem; border-width: 1px">
    <!-- like,comment,repost section -->
    <div class='mb-1'>
        <a href="" class="float-right mr-2 mb-2" data-toggle="modal" data-target=<?="#repostModal-".$post['Post']['id'] ?> >Repost</a>
        <div class="modal fade" id=<?="repostModal-".$post['Post']['id'] ?> tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Repost</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                    <div class="mb-3">
                        <label for="message-text" class="col-form-label">Add to the post:</label>
                        <textarea id=<?= "repost-".$post['Post']['id'] ?> rows="5" maxLength ='140' class="col-12" placeholder="What's on your mind?"></textarea>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary repost" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $post['Post']['id'] ?>>Repost</button>
                </div>
                </div>
            </div>
        </div>
        <?= $this->Html->link(
            'Comment',
            array('controller' => 'posts','action' => 'view', $post['Post']['id']),
            array('class' => 'card-text float-right mr-2 mb-2')
        )?>
        <!-- Checking if the user has liked the post -->
        <?php
        if (empty($post['Like'])) {
            $like = 'Like';
        } else {
            for ($i = 0; $i < count($post['Like']); $i++) {
                if ($post['Like'][$i]['user_id'] === $this->Session->read('Auth.User.id')) {
                    $like = "Liked";
                    break;
                } else {
                    $like = "Like";
                }
            }
        }
        ?>
        <!-- Checking if the user has liked the post -->
        <a href="" class="float-right mr-2 mb-2 like" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $post['Post']['id'] ?>><?= $like ?></a>
    </div>
    <!-- end of like,comment,repost section -->
    </div>
<?php endforeach;?>
</div>

<!-- followers view section -->
<div id="showFollowers" class="card border-primary mx-auto mt-3"  style="max-width: 35rem;display:none;">
    <h4 class='my-3' align='center'><strong><?= ucfirst($user['User']['first_name'])." ".ucfirst($user['User']['last_name'])."'s Followers" ?></strong></h4>
    <?php foreach ($followers as $follower) : ?>
    <div class="card-header my-2" style="border-width: 1px;" id= <?= "commentDiv-".$follower['User']['id']?>>
        <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$follower['User']['id'] )) ?>>
        <?= $this->Html->image(
            $follower['User']['image_location'],
            array('width' => '50','height' => '50',
            'class' => 'float-left mr-1 ')
        ) ?>
        <h5><?= ucfirst($follower['User']['first_name'])." ".ucfirst($follower['User']['last_name']) ?></h5>
        <h6 class="card-subtitle text-muted">@<?= $follower['User']['username'] ?></h6>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $follower['User']['id']) : ?>
            <?php (in_array($follower['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
        <button class="btn btn-outline-primary btn-sm float-right follow-button" style="position: relative; right: 10px;bottom: 20px;" id=<?= $follower['User']['id'] ?>><?= $follow ?></button><br>
        <?php endif ?>
    </div>
    <?php endforeach ?>
</div>

<!-- followings view section -->
<div id="showFollowings" class="card border-primary mx-auto mt-3"  style="max-width: 35rem;display:none;">
    <h4 class='my-3' align='center'><strong><?= ucfirst($user['User']['first_name'])." ".ucfirst($user['User']['last_name'])."'s Following List" ?></strong></h4>
    <?php foreach ($followings as $follows) : ?>
    <div class="card-header my-2" style="border-width: 1px;" id= <?= "commentDiv-".$follows['User']['id']?>>
        <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$follows['User']['id']))?>>
        <?= $this->Html->image(
            $follows['User']['image_location'],
            array('width' => '50','height' => '50',
            'class' => 'float-left mr-1 ')
        ) ?>
        <h5><?= ucfirst($follows['User']['first_name'])." ".ucfirst($follows['User']['last_name']) ?></h5>
        <h6 class="card-subtitle text-muted">@<?= $follows['User']['username'] ?></h6>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $follows['User']['id']) : ?>
            <?php (in_array($follows['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
        <button class="btn btn-outline-primary btn-sm float-right follow-button" style="position: relative; right: 10px;bottom: 20px;" id=<?= $follows['User']['id'] ?>><?= $follow ?></button><br>
        <?php endif ?>
    </div>
    <?php endforeach ?>
</div>

<!-- pagination of posts section -->
<div class="mx-auto" style="max-width: 35rem;">
    <div class="mb-4 float-right">
        <?= $paginator->first(" << ") ?>
        <?= $paginator->hasPrev() ? $paginator->prev(" Previous ") : '' ?>
        <?= $paginator->numbers(array('modulus' => 5)) ?>
        <?= $paginator->hasNext() ? $paginator->next(" Next ") : '' ?>
        <?= $paginator->last(" >> ") ?>
    </div>
</div>

<!-- alerts -->
<div id="myAlert" class="alert alert-success collapse" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close">&times</a>
    <?php $alert_message = $this->Session->read('alert_message');
    unset($_SESSION['alert_message']); ?>
    <strong id ="alert-message"><?= empty($alert_message) ? '' : $alert_message ?></strong>

</div>
