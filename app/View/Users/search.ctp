<?php
    $paginator = $this->Paginator;
?>

<div class="card border-primary mx-auto my-5"  style="max-width: 35rem;">
    <h4 class='my-3' align='center'>Search results for: <strong><?= $search ?></strong></h4>
</div>

<?php foreach ($results as $result) : ?>
<div id="showSearchResults" class="card border-primary mx-auto my-3"  style="max-width: 35rem;">
    <div class="card-header" style="border-width: 1px;" id= <?= "commentDiv-".$result['User']['id']?>>
        <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$result['User']['id'])) ?>>
        <?= $this->Html->image(
            $result['User']['image_location'],
            array('width' => '60','height' => '60',
            'class' => 'float-left mr-1 ')
        ) ?>
        <h5><?= ucfirst($result['User']['first_name'])." ".ucfirst($result['User']['last_name']) ?></h5>
        <h6 class="card-subtitle text-muted">@<?= $result['User']['username'] ?></h6>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $result['User']['id']) : ?>
            <?php (in_array($result['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
        <button class="btn btn-outline-primary btn-sm float-right follow-button" style="position: relative; right: 10px;bottom: 20px;" id=<?= $result['User']['id'] ?>><?= $follow ?></button><br>
        <?php endif ?>
    </div>
</div>
<?php endforeach ?>

<!-- pagination section -->
<div class="mx-auto" style="max-width: 35rem;">
    <div class="mb-4 float-right">
        <?= $paginator->first(" << ") ?>
        <?= $paginator->hasPrev() ? $paginator->prev(" Previous ") : '' ?>
        <?= $paginator->numbers(array('modulus' => 5)) ?>
        <?= $paginator->hasNext() ? $paginator->next(" Next ") : '' ?>
        <?= $paginator->last(" >> ") ?>
    </div>
</div>

<!-- alerts -->
<div id="myAlert" class="alert alert-success collapse" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close">&times</a>
    <?php $alert_message = $this->Session->read('alert_message');
    unset($_SESSION['alert_message']); ?>
    <strong id ="alert-message"><?= empty($alert_message) ? '' : $alert_message ?></strong>

</div>
