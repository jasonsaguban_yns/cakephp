<?php
$cakeDescription = __d('cake_dev', 'MicroBlog');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <?php
        echo $this->Html->meta('icon');
        // echo $this->Html->css('cake.generic');
        echo $this->Html->css('styles');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
<?php if ($this->Session->read('Auth.User')) : ?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
  <div class="container-fluid">
      <strong>
    <?= $this->Html->link(
        'Micro Blog',
        array(
        'controller' => 'posts',
        'action' => 'index'
        ),
        array(
        'class' => 'navbar-brand col-4 mr-5'
        )
    ) ?>
    </strong>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <form class="d-flex mx-auto col-6">
        <?php $searchText = $this->Session->read('Searchtext');unset($_SESSION['Searchtext']) ?>
        <input class="form-control mr-2 form-control-sm" id="searchInput" type="search" placeholder="Search" aria-label="Search" value=<?= (!empty($searchText) ? $searchText : '') ?>>
        <a href='' class="btn btn-primary btn-sm" type="submit" id="searchBtn"><i class="fa fa-search"></i></a>
      </form>

      <ul class="navbar-nav ">
        <li class="nav-item active">
            <a href=<?= $this->Html->url(array('controller' =>'posts', 'action' => 'index')) ?> class = 'nav-link'>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                </svg>
                Home
            </a>
        </li>
        <li class="nav-item active">
            <?= $this->Html->link(
                'Profile',
                array(
                'controller' => 'users',
                'action' => 'view',
                $this->Session->read('Auth.User.id')
                ),
                array(
                'class' => 'nav-link',
                )
            ) ?>                       
        </li>
        <li class="nav-item ">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <?= "Hi ". ucfirst($this->Session->read('Auth.User.first_name'))."!" ?>
                </button>
                <ul class="dropdown-menu dropdown-menu-right mt-2">
                    <li>
                        <?= $this->Html->link(
                            'Edit Profile',
                            array(
                                'controller' => 'users',
                                'action' => 'edit',
                                $this->Session->read('Auth.User.id')
                            ),
                            array(
                                'class' => 'dropdown-item',
                            )
                        ) ?>   
                    </li>
                    <li>
                        <?= $this->Html->link(
                            'Logout',
                            array(
                                'controller' => 'users',
                                'action' => 'logout'
                            ),
                            array(
                                'class' => 'dropdown-item text-danger',
                            )
                        ) ?>   
                    </li>
                </ul>
            </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>
<?php endif ?>

    <div class="loader-wrapper">
        <span class="loader"><span class="loader-inner"></span></span>
    </div>

    <div id="content" style="display:none">
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
<?= $this->Html->script('script') ?>
</body>
</html>
