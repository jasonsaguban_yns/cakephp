<div id="<?php echo h($key) ?>Message" class="alert alert-success" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close" data-dismiss="alert">&times</a>
    <strong id ="alert-message"><?php echo h($message) ?></strong>
</div>