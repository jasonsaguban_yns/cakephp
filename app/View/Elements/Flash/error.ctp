<div id="<?= $key ?>Message" class="alert alert-danger" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close" data-dismiss="alert">&times</a>
    <strong id ="alert-message"><?= $message ?></strong>
</div>