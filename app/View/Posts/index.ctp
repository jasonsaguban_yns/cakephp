<?php $paginator = $this->Paginator;
 ?>

<!-- Adding post section -->
<div id='divAddPost' class="mt-5 mx-auto" style="max-width: 35rem;">
    <textarea class="mt-5 col-10" id="addPostTextArea" cols="30" rows="5" required='required' maxLength = '140' placeholder="<?= "What's on your mind, ".ucfirst($this->Session->read('Auth.User.first_name'))."?"?>" ></textarea>
    <button id="addPostBtn" class="btn btn-primary float-right mt-5">Post</button>
    <br>   
    <small id="charCounter"></small>
</div>

<hr class="bg-primary" style="max-width: 35rem; border-width: 5px">
<!-- posts view section -->
<?php foreach ($posts as $key => $post) : ?>
    <?php
        $noOfComments = 0;
        for ($i = 0; $i < count($post['Comment']); $i++) {
            if ($post['Comment'][$i]['deleted'] == '') {
                $noOfComments++;
            }
        }
            $noOfRepost = 0;
        for ($i = 0; $i < count($post['Repost']); $i++) {
            if ($post['Repost'][$i]['deleted'] == '') {
                $noOfRepost++;
            }
        }
        $postId = $post['Post']['id'];
        $postUserId = $post['Post']['user_id'];
        $repostId = $post['Reposts']['id'];
    ?>
    <div class="card border-primary my-4 mx-auto" id=<?= "card-post-".$postId ?> style="max-width: 35rem;">
        <!-- header part of post section-->
        <div class="card-header ">
            <!-- actions to the post -->
            <button class="btn btn-sm btn-danger float-right" style="display:none;" id=<?="cancelBtn-".$postId?>>Cancel</button>
            <div class="btn-group dropright float-right post-actions" >
                <button class="btn btn-primary dropdown-toggle dropdown-toggle-split btn-sm" data-toggle="dropdown">
                </button>
                <ul class="dropdown-menu">
                    <?php if ($postUserId === $this->Session->read('Auth.User.id')) : ?>
                        <li>
                            <a href="#" class="dropdown-item delete-post" onclick="if (confirm('Are you sure you want to delete this post?') ) {return true;} else{ return false;}" id=<?= $postId ?> 
                            name=<?= $repostId ?>>
                                Delete
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-item edit-post" id=<?= $postId ?>>Edit</a>
                        </li>
                    <?php endif ?>    
                    <li>
                        <?= $this->Html->link(
                            'View Post',
                            array('action' => 'view', $postId),
                            array('class' => 'dropdown-item')
                        )?>
                    </li>
                </ul>        
            </div>
            <!-- post user informations -->
            <div>
                <a href=<?= $this->Html->url(array('controller' => 'users' , 'action' => 'view' , $postUserId))?>>
                <?= $this->Html->image($post['User']['image_location'], array('width' => '60','height' => '60','class' => 'float-left mr-1 ')) ?>
                <?= ucfirst($post['User']['first_name'])." ".ucfirst($post['User']['last_name']) ?>
                <h6 class="card-subtitle text-muted">@<?= $post['User']['username'] ?></h6>
                </a>                
                <p class="card-text" style="display:inline"><small class="text-muted"><?= date('F j, Y, g:i a', strtotime($post['Post']['created'])) ?></small></p>
                <?php if ($this->Session->read('Auth.User.id') !== $post['User']['id']) : ?>
                    <?php (in_array($post['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User']['id'] ?>><?= $follow ?></button><br>
                <?php endif ?>

            </div>
        </div>
        
        <!-- contents of the posts section -->
        <div class="card-body text-black">
            <p class= 'card-text mx-3' id=<?= 'post-'.$postId ?> ><?= $post['Post']['post'] ?></p>
            <!-- editting of post section -->
            <div style="display: none;" class = "mb-5" id = <?= "edit-post".$postId ?>>
                <textarea name="editPost" id=<?= "textarea-".$postId ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
                <input type="hidden" name="postId" value=<?= $postId ?>>
                <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$postId ?>>Save Changes</button>
                <small class="ml-2" id=<?= "charCounter-".$postId ?>></small>
            </div>

            <!-- card for original post -->
            <?php if (!empty($post['Post_2']['id'])) : ?>
            <div class="card border-secondary">
                <div class='card-header'>
                    <a href=<?= $this->Html->url(array('controller' => 'users' , 'action' => 'view' , $post['User_2']['id'])) ?>>
                    <?= $this->Html->image($post['User_2']['image_location'], array('width' => '50','height' => '50','class' => 'float-left mr-1 ')) ?>
                    <small>
                    <?= ucfirst($post['User_2']['first_name'])." ".ucfirst($post['User']['last_name']) ?></small>
                    <small class="card-subtitle text-muted">@<?= $post['User_2']['username'] ?></small><br>
                    </a>
                    <small class="text-muted"  style="display:inline"><?= date('F j, Y, g:i a', strtotime($post['Post_2']['created'])) ?></small>
                    <?php if ($this->Session->read('Auth.User.id') !== $post['User_2']['id']) : ?>
                        <?php (in_array($post['User_2']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                    <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User_2']['id'] ?>><?= $follow ?></button><br>
                    <?php endif ?>
                </div>
                <div class="card-body text-secondary">
                <small>
                    <a href=<?= $this->Html->url(array('controller' => 'posts','action' => 'view',$post['Post_2']['id'])) ?>  class="float-right"><?= (empty($post['Post_2']['deleted'])) ?  'View Original Post' : '' ?>
                    </a>
                </small>
                <p class= 'card-text mt-3'><?= (empty($post['Post_2']['deleted'])) ?  $post['Post_2']['post'] :  '<strong><i>THIS POST WAS DELETED</i></strong>' ?></p>
                </div>
            </div>
            <?php endif ?>
        </div>

        <!-- number of likes,comments,reposts section -->
        <div class="mx-3 text-secondary">
            <p class="card-text mx-1" style="display: inline">[Likes 
                <span id=<?= "noOfLikes".$postId ?>><?= count($post['Like']) ?></span>]
            </p>
            <p class="card-text mx-1" style="display: inline">[Comments 
                <span id=<?= "noOfComments".$postId ?>><?= $noOfComments ?></span>]
            </p>
            <p class="card-text mx-1" style="display: inline">[Reposts 
                <span id=<?= "noOfReposts".$postId ?>><?= $noOfRepost ?></span>]
            </p>
        </div>

        <!-- like,comment,repost section -->
        <hr class="bg-primary" style="max-width: 35rem; border-width: 1px">
        <div class="mb-1">
            <a href="" class="float-right mr-2 mb-2" data-toggle="modal" data-target=<?="#repostModal-".$postId ?> >Repost</a>
            <div class="modal fade" id=<?="repostModal-".$postId ?> tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Repost</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Add to the post:</label>
                            <textarea id=<?= "repost-".$postId ?> rows="5" maxLength ='140' class="col-12" placeholder="What's on your mind?"></textarea>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary repost" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $postId ?>>Repost</button>
                    </div>
                    </div>
                </div>
            </div>
            <?= $this->Html->link(
                'Comment',
                array('action' => 'view', $postId),
                array('class' => 'card-text float-right mr-2 mb-2')
            )?>
            <!-- Checking if the user has liked the post -->
            <?php
                if (empty($post['Like'])) {
                    $like = 'Like';
                } else {
                    for ($i = 0; $i < count($post['Like']); $i++) {
                        if ($post['Like'][$i]['user_id'] === $this->Session->read('Auth.User.id')) {
                            $like = "Liked";
                            break;
                        } else {
                            $like = "Like";
                        }
                    }
                }
            ?>
            <a href="" class="float-right mr-2 mb-2 like" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $postId ?>><?= $like ?></a>
        </div>

    </div>
<?php endforeach;?>

<!-- pagination of posts section -->
<div class="mx-auto" style="max-width: 35rem;">
    <div class="mb-4 float-right">
        <?= $paginator->first(" << ") ?>
        <?= $paginator->hasPrev() ? $paginator->prev(" Previous ") : '' ?>
        <?= $paginator->numbers(array('modulus' => 5)) ?>
        <?= $paginator->hasNext() ? $paginator->next(" Next ") : '' ?>
        <?= $paginator->last(" >> ") ?>
    </div>
</div>


<!-- alerts -->
<div id="myAlert" class="alert alert-success collapse" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close">&times</a>
    <?php $alert_message = $this->Session->read('alert_message');
    unset($_SESSION['alert_message']); ?>
    <strong id ="alert-message"><?= empty($alert_message) ? '' : $alert_message ?></strong>
</div>
