<?php
    $noOfComments=0;
    for ($i = 0; $i < count($post['Comment']); $i++) {
        if ($post['Comment'][$i]['deleted'] == '') {
            $noOfComments++;
        }
    }
        $noOfReposts=0;
    for ($i = 0; $i < count($post['Repost']); $i++) {
        if ($post['Repost'][$i]['deleted'] == '') {
            $noOfReposts++;
        }
    }
    $postId = $post['Post']['id'];
    $postUserId = $post['Post']['user_id'];
    $paginator = $this->Paginator;
?>
<!-- post view section -->
<div class="card border-primary mx-auto" style="max-width: 35rem;margin-top: 70px;">      
    <!-- header part of post section-->
    <div class="card-header ">
        <!-- actions to the post -->
        <div>
            <button class="btn btn-sm btn-danger float-right" style="display: none;" id=<?= "cancelBtn-".$postId?>>Cancel</button>
            <?php if ($postUserId === $_SESSION['Auth']['User']['id']) : ?>
            <div class="btn-group dropright float-right post-actions">
                <button class="btn btn-primary dropdown-toggle dropdown-toggle-split btn-sm" data-toggle="dropdown">
                </button>
                <ul class="dropdown-menu">            
                    <li>
                        <?= $this->Html->link(
                            'Delete',
                            array('controller' => 'posts','action' => 'delete', $postId,$post['Post']['repost_id'],'?fromview=1'),
                            array('class' => 'dropdown-item', 'confirm' => 'Are you sure you wish to delete this post?')
                        ) ?>
                    </li>
                    <li>
                        <a href="#" class="dropdown-item edit-post" id=<?= $postId ?>>Edit</a>
                    </li>                    
                </ul>        
            </div>
            <?php endif ?>
        </div>

        <!-- post user informations -->
        <div>        
            <a href=<?= $this->Html->url(array('controller' => 'users', 'action' => 'view',$post['User']['id'])) ?>>
            <?= $this->Html->image(
                $post['User']['image_location'],
                array('width' => '60','height' => '60',
                'class' => 'float-left mr-1 ')
            ) ?>
            <?= ucfirst($post['User']['first_name'])." ".ucfirst($post['User']['last_name']) ?>
            <h6 class="card-subtitle text-muted">@<?= $post['User']['username'] ?></h6>
            </a>
            <p class="card-text" style="display:inline"><small class="text-muted"><?= date('F j, Y, g:i a', strtotime($post['Post']['created'])) ?></small></p>
            <?php if ($this->Session->read('Auth.User.id') !== $post['User']['id']) : ?>
                <?php (in_array($post['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User']['id'] ?>><?= $follow ?></button><br>
            <?php endif ?>
        </div>
    </div>
    
    <!-- contents of the posts section -->
    <div class="card-body text-black">
        <p class= 'card-text mx-3' id=<?= 'post-'.$postId ?> ><?= $post['Post']['post'] ?></p>

        <!-- editting of post section -->
        <div style="display: none;" class = "mb-5" id = <?= "edit-post".$postId ?>>
            <textarea name="editPost" id=<?= "textarea-".$postId ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
            <input type="hidden" name="postId" value=<?= $postId ?>>
            <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$postId ?>>Save Changes</button>
            <small class="ml-2" id=<?= "charCounter-".$postId ?>></small>
        </div>

        <!-- card for original post -->
        <?php if (!empty($post['Post_2']['id'])) : ?>
        <div class="card border-secondary">
            <div class='card-header'>
                <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$post['User_2']['id'])) ?>>
                <?= $this->Html->image($post['User_2']['image_location'], array('width' => '50','height' => '50','class' => 'float-left mr-1 ')) ?>
                <small>
                <?= ucfirst($post['User_2']['first_name'])." ".ucfirst($post['User']['last_name']) ?></small>
                <small class="card-subtitle text-muted">@<?= $post['User_2']['username'] ?></small><br>
                </a>
                <small class="text-muted"><?= date('F j, Y, g:i a', strtotime($post['Post_2']['created'])) ?></small>
                <?php if ($this->Session->read('Auth.User.id') !== $post['User_2']['id']) : ?>
                    <?php (in_array($post['User_2']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User_2']['id'] ?>><?= $follow ?></button><br>
                <?php endif ?>
            </div>
            <div class="card-body text-secondary">
            <small>
                <a href=<?= $this->Html->url(array('controller' => 'posts','action' => 'view',$post['Post_2']['id'])) ?> class="float-right"><?= (empty($post['Post_2']['deleted'])) ?  'View Original Post' : '' ?>
                </a>
            </small>
            <p class= 'card-text mt-3'><?= (empty($post['Post_2']['deleted'])) ?  $post['Post_2']['post'] :  '<strong><i>THIS POST WAS DELETED</i></strong>' ?></p>
            </div>
        </div>
        <?php endif ?>
    </div>

    <!-- number of likes,comments,reposts section -->    
    <div class="mx-3 text-secondary">
        <p class="card-text mx-1" style="display: inline">[Likes 
            <span id=<?= "noOfLikes".$postId?>><?= count($post['Like']) ?></span>]
        </p>
        <p class="card-text mx-1" style="display: inline">[Comments 
            <span id="noOfComments"><?= $noOfComments ?></span>]
        </p>
        <p class="card-text mx-1" style="display: inline">[Reposts 
            <span id=<?= "noOfReposts".$postId ?>><?= $noOfReposts ?></span>]
        </p>
    </div>    
    <hr class="bg-primary" style="max-width: 35rem; border-width: 1px">
    <!-- like,comment,repost section -->
    <div class='mb-1'>
        <a href="" class="float-right mr-2 mb-2" data-toggle="modal" data-target=<?="#repostModal-".$postId ?> >Repost</a>
            <div class="modal fade" id=<?="repostModal-".$postId ?> tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Repost</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Add to the post:</label>
                            <textarea id=<?= "repost-".$post['Post']['id'] ?> rows="5" maxLength ='140' class="col-12" placeholder="What's on your mind?"></textarea>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary repost" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $post['Post']['id'] ?>>Repost</button>
                    </div>
                    </div>
                </div>
            </div>
        <?= $this->Html->link(
            'Comment',
            array( 'action' => 'view', $post['Post']['id']),
            array('class' => 'card-text float-right mr-2 mb-2','id' => 'commentClick')
        )?>

        <!-- Checking if the user has liked the post -->
        <?php
        if (empty($post['Like'])) {
            $like = 'Like';
        } else {
            for ($i = 0; $i < count($post['Like']); $i++) {
                if ($post['Like'][$i]['user_id'] === $this->Session->read('Auth.User.id')) {
                    $like = "Liked";
                    break;
                } else {
                    $like = "Like";
                }
            }
        }
        ?>

        <a href="" class="float-right mr-2 mb-2 like" id=<?= $this->Session->read('Auth.User.id')?> name= <?= $postId ?>><?= $like ?></a>
    </div>
</div>

<!-- Adding comment section -->
<div class="mx-auto" style="max-width: 35rem;">
    <?php
    echo $this->Form->create(false, array(
        'url' => array('controller' => 'Comments', 'action' => 'add'),
        'id' => 'CommentAddForm'
    ));
    echo $this->Form->hidden('user_id', array('value' => $this->Session->read('Auth.User.id')));
    echo $this->Form->hidden('post_id', array('value' => $postId));
    echo $this->Form->textarea('comment', array(
        'maxLength' => '140',
        'class' => 'mt-5 col-12',
        'rows' => '4',
        'required' => 'required',
        'placeholder' => "What's your comment on this, ".ucfirst($this->Session->read('Auth.User.first_name'))."?"));
    echo $this->Form->button('Comment', array(
        'type' => 'submit' ,
        'class' => 'btn btn-primary float-right'));
    echo $this->Form->end();
    ?>
    <br>   
    <small id="charCounter"></small>
</div>

<!-- comments view section -->
<?php if (!empty($comments)) : ?>
<div class="card border-primary mx-auto mb-3" style="max-width: 35rem;margin-top: 55px;">
    <?php foreach ($comments as $comment) : ?>
        <?php $commentId = $comment['Comment']['id']; ?>
        <div class="card-header" style="border-width: 1px;" id= <?= "commentDiv-".$commentId ?>>
            <!-- actions to the comment -->
            <div>
                <button class="btn btn-sm btn-danger float-right" style="display: none;" id=<?= "cancelBtn-".$commentId?>>Cancel</button>
                <div class="commentActions">
                    <?php if ($comment['Comment']['user_id'] == $_SESSION['Auth']['User']['id']) : ?>
                        <div class="btn-group dropright float-right">
                            <button class="btn btn-primary dropdown-toggle dropdown-toggle-split btn-sm" data-toggle="dropdown">
                            </button>
                            <ul class="dropdown-menu">                    
                                <a href="" class="dropdown-item edit" id=<?= $commentId ?>>
                                    Edit
                                </a>                        
                                <a href="" class="dropdown-item delete" onclick="if (confirm('Are you sure you want to delete this comment?') ) {return true;} else{ return false;}" id=<?= $commentId ?>>
                                    Delete
                                </a>                            
                            </ul>        
                        </div>
                    <?php endif ?>  
                </div>
            </div>

            <!-- comment user informations -->
            <div>
            
                <a href=<?= $this->Html->url(array('controller' => 'users','action' => 'view',$comment['User']['id'])) ?>>
                <?= $this->Html->image(
                    $comment['User']['image_location'],
                    array('width' => '60','height' => '60',
                    'class' => 'float-left mr-1 ')
                ) ?>
                <?= ucfirst($comment['User']['first_name'])." ".ucfirst($comment['User']['last_name']) ?>
                <h6 class="card-subtitle text-muted">@<?= $comment['User']['username'] ?></h6>
                </a>
                <p class="card-text" style="display:inline"><small class="text-muted"><?= date('F j, Y, g:i a', strtotime($comment['Comment']['modified'])) ?></small></p>
                <?php if ($this->Session->read('Auth.User.id') !== $comment['User']['id']) : ?>
                    <?php (in_array($comment['User']['id'], $following)) ? $follow = 'Following' : $follow = 'Follow'?>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="display:inline" id=<?= $post['User']['id'] ?>><?= $follow ?></button><br>
                <?php endif ?>
                <hr class="bg-primary">
            </div>

            <!-- contents of the comment section -->
            <p class= <?= 'card-text'.$commentId ?>><?= $comment['Comment']['comment'] ?></p>

        </div>
        <!-- editting of comment section -->
        <div style="display: none;" class = <?= "edit-comment".$commentId ?>>
            <textarea name="editComment" id=<?= "textarea-".$commentId ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
            <input type="hidden" name="commentId" value=<?= $commentId ?>>
            <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$commentId ?>>Save Changes</button>
            <small class="ml-2" id=<?= "charCounter-".$commentId ?>></small>
        </div>
    <?php endforeach ?>
    <!-- pagination of comments section -->
    <div>
        <div class="mt-2 mb-2 float-right">
            <?= $paginator->first(" << ") ?>
            <?= $paginator->hasPrev() ? $paginator->prev(" Previous ") : '' ?>
            <?= $paginator->numbers(array('modulus' => 4)) ?>
            <?= $paginator->hasNext() ? $paginator->next(" Next ") : '' ?>
            <?= $paginator->last(" >> ") ?>
        </div>
    </div>
    <!-- end of pagination of comments sections -->
</div>
<?php endif ?>

<!-- alerts -->
<div id="myAlert" class="alert alert-success collapse" style="position: fixed; bottom: 10px;right:10px;width:300px;">
    <a id="alertClose" href="#" class="close">&times</a>
    <?php $alert_message = $this->Session->read('alert_message');
    unset($_SESSION['alert_message']); ?>
    <strong id ="alert-message"><?= empty($alert_message) ? '' : $alert_message ?></strong>
</div>
